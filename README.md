# This is my package laravel-translate

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-translate.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-translate)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-translate.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-translate)

## Installation

You can install the package via composer:

```bash
composer require kda/laravel-translate
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="\KDA\Laravel\Translate\ServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="\KDA\Laravel\Translate\ServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
];
```

Optionally, you can publish the views using

```bash
php artisan vendor:publish --provider="\KDA\Laravel\Translate\ServiceProvider" --tag="views"
```

## Usage

```php
$laravel\Translate = new KDA\Laravel\Translate();
echo $laravel\Translate->echoPhrase('Hello, KDA!');
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/fdt2k/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
