<?php
namespace KDA\Laravel\Translate;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Translate\Facades\TranslateManager as Facade;
use KDA\Laravel\Translate\TranslateManager as Library;
    use KDA\Laravel\Traits\HasConfig;
    use KDA\Laravel\Traits\HasLoadableMigration;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasLoadableMigration;
    protected $packageName ='laravel-translate';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
    protected $configDir='config';
    protected $configs = [
         'kda/translate.php'  => 'kda.translate'
    ];
    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
