<?php

namespace KDA\Laravel\Translate\Models\Traits;

use KDA\Laravel\Translate\Models\Translation;

trait HasTranslations
{

    public static function bootHasTranslations(): void
    {
        static::creating(function ($model)
        {
        });
        static::updating(function ($model)
        {
        });
    }

    

    public function translated()
    {
        return $this->morphMany(Translation::class, 'model');
    }
/*
    public function has_one_of_many_polymorphic()
    {
        return $this->morphOne(Slug::class, 'sluggable')->latestOfMany();
    }

    public function scopeWithNoSlug($query)
    {
        return $query->whereDoesntHave('slugs');
     }*/
}
