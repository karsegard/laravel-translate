<?php

namespace KDA\Laravel\Translate\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Translation extends Model
{
    use HasFactory;

    protected $fillable = [
        'model_type',
        'model_id',
        'attribute',
        'locale',
        'value'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       
    ];

   
    protected static function newFactory()
    {
        return  \KDA\Laravel\Translate\Database\Factories\TranslationFactory::new();
    }

}
