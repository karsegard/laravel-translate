<?php

namespace KDA\Laravel\Translate\Facades;

use Illuminate\Support\Facades\Facade;

class TranslateManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
