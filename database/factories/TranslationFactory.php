<?php

namespace KDA\Laravel\Translate\Database\Factories;

use KDA\Laravel\Translate\Models\Translation;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Tests\Models\Post;

class TranslationFactory extends Factory
{
    protected $model = Translation::class;

    public function definition()
    {
        $post = Post::factory()->create([]);
        return [
            'model_type'=>get_class($post),
            'model_id'=>$post->getKey(),
            'attribute'=>'title',
            'locale'=>$this->faker->locale(),
            'value'=>$this->faker->sentence(4)
        ];
    }
}
